#Bomberman

Minigioco da terminale multigiocatore scritto in Java.  

Per avviare il progetto:  

1) installa il modulo "shared" tramite: mvn compile install  
2) installa il modulo "simulation" tramite mvn compile install  
3) avvia i moduli client server tramite: mvn compile exec:java  
