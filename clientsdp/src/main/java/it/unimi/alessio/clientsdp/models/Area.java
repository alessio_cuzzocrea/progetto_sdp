package it.unimi.alessio.clientsdp.models;

/**
 * Created by alessio on 24/06/17.
 */
public enum  Area {
    VERDE,
    ROSSA,
    BLUE,
    GIALLA;

    public static Area valueOf(int ordinal){
        for (Area c: Area.values()) {
            if(c.ordinal() == ordinal)
                return c;
        }
        throw new IllegalArgumentException();
    }
}
