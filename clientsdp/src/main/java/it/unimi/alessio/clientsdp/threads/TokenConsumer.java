package it.unimi.alessio.clientsdp.threads;

import it.unimi.alessio.clientsdp.AppProperties;
import it.unimi.alessio.clientsdp.PlayingCommand;
import it.unimi.alessio.clientsdp.PropertiesFields;
import it.unimi.alessio.clientsdp.RestClient;
import it.unimi.alessio.clientsdp.messages.*;
import it.unimi.alessio.clientsdp.models.Bomb;
import it.unimi.alessio.clientsdp.states.DeathState;
import it.unimi.alessio.clientsdp.states.MatchContext;
import it.unimi.alessio.clientsdp.storages.ResponseMessageStorage;
import it.unimi.alessio.clientsdp.storages.Storage;

import java.util.List;

/**
 * Created by alessio on 14/06/17.
 */
public class TokenConsumer extends Thread {
    private MatchContext matchContext;
    private Storage<PlayingCommand> userInputStorage;
    private Storage<Message> messageStorage;

    public TokenConsumer(MatchContext matchContext) {
        this.matchContext = matchContext;
        userInputStorage = this.matchContext.getUserInputStorage();
        userInputStorage.subscribe();
        messageStorage = matchContext.getMessageStorage();
    }

    @Override
    public void run() {
        int sleepFactor = Integer.parseInt(AppProperties.getProperties().getProperty(PropertiesFields.sleepFactor));
        boolean printConsumingToken = Boolean.parseBoolean(AppProperties.getProperties().getProperty(PropertiesFields.printConsumingToken));
        boolean debugEnabled = Boolean.parseBoolean(AppProperties.getProperties().getProperty(PropertiesFields.debugMode));
        while (true) {
            //wait token
            matchContext.getTokenStorage().checkInToken();
            if (matchContext.getState() instanceof DeathState) {
                matchContext.bootstrapState();
                break;
            }

            if (userInputStorage.hasItem()) {
                PlayingCommand cmd = userInputStorage.peekItem();
                cmd.execute(matchContext);
                if(cmd.equals(PlayingCommand.DROP_BOMB))
                    dropBomb();
                else
                    executeMove();

            }
            if (printConsumingToken) {
                System.out.println("consuming token");

            }
            try {
                Thread.sleep(sleepFactor);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            matchContext.getTokenStorage().checkOutToken();
        }

        Message announceDeath = new Message(
                MessageType.ANNOUNCE_PLAYER_DEATH,
                matchContext.getCurrentPlayer(),
                null
        );
        int expectedResponses = matchContext.getCurrentGame().getUserCount() - 1;
        ResponseMessageStorage rms = new ResponseMessageStorage(expectedResponses);
        matchContext.setResponseMessageStorage(rms);
        matchContext.getMessageStorage().putItem(announceDeath);
        RestClient rc = matchContext.getRestClient();
        rc.removePlayerFromGame(matchContext.getCurrentGame().getName(), matchContext.getCurrentPlayer());
        if(debugEnabled)
            System.out.println("waiting " + expectedResponses +" responses");
        matchContext.getResponseMessageStorage().awaitResponses();

        matchContext.getMessageStorage().putItem(
                new Message(
                        MessageType.TOKEN_MESSAGE,
                        matchContext.getCurrentPlayer(),
                        matchContext.getNextPlayer()
                )
        );
        System.out.println("punteggio finale: " + matchContext.getPoints());
        matchContext.getMessageStorage().waitToBeFree();
        System.exit(1);
    }

    private void dropBomb() {
        Bomb bomb = matchContext.getBombList().getFirst();
        if(bomb == null) {
            userInputStorage.getItem();
            return;
        }
        boolean debugEnabled = Boolean.parseBoolean(AppProperties.getProperties().getProperty(PropertiesFields.debugMode));
        if(debugEnabled)
            System.out.println("sending bomb message");
        MessageWithBomb message = new MessageWithBomb(
                MessageType.BOMB_MESSAGE,
                matchContext.getCurrentPlayer(),
                null,
                bomb
        );
        int expectedResponses = matchContext.getCurrentGame().getUserCount() - 1;
        matchContext.setResponseMessageStorage(new ResponseMessageStorage(expectedResponses));
        matchContext.getMessageStorage().putItem(message);
        matchContext.getResponseMessageStorage().awaitResponses();
        if(debugEnabled)
            System.out.println("everyone has replied");
        new BombExplodedThread(bomb, matchContext).start();

        userInputStorage.getItem();
    }

    private void executeMove() {
        boolean debugEnabled = Boolean.parseBoolean(AppProperties.getProperties().getProperty(PropertiesFields.debugMode));
        if(debugEnabled)
            System.out.println("sending move message");
        MessageWithCoordinates message = new MessageWithCoordinates(
                MessageType.MOVE_MESSAGE,
                matchContext.getCurrentPlayer(),
                null,
                matchContext.getCurrentPosition()
        );
        int expectedResponses = matchContext.getCurrentGame().getUserCount() - 1;
        matchContext.setResponseMessageStorage(new ResponseMessageStorage(expectedResponses));
        matchContext.getMessageStorage().putItem(message);
        List<Message> responseList = matchContext.getResponseMessageStorage().awaitResponses();
        if(debugEnabled)
            System.out.println("responses arrived");
        //check if anyone has died
        for (Message m : responseList) {
            if (m.getType().equals(MessageType.MOVE_RESPONSE_DIED)) {
                matchContext.incrementPoints();
            }
        }
        matchContext.checkVictory();
        userInputStorage.getItem();
    }
}

