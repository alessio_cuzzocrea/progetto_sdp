package it.unimi.alessio.clientsdp.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alessio on 17/06/17.
 */
public class SynchronizedList<T> {
    private final ArrayList<T> list;
    public SynchronizedList() {
        this.list = new ArrayList<>();
    }

    public synchronized boolean remove(T item){
        return list.remove(item);
    }
    public synchronized void add(T item){
        list.add(item);
    }
    public synchronized T get(int index){
        return list.get(index);
    }
    public synchronized T get(T obj){
        return  get(list.indexOf(obj));
    }
}
