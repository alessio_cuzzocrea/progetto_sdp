package it.unimi.alessio.clientsdp.messages;

import it.unimi.alessio.clientsdp.models.BombExploded;
import it.unimi.alessio.shared.models.Player;

/**
 * Created by alessio on 26/06/17.
 */
public class MessageWithBombExploded extends Message{
    private BombExploded bombExploded;
    public MessageWithBombExploded(MessageType type, Player sender, Player recipient, BombExploded bombExploded) {
        super(type, sender, recipient);
        this.bombExploded = bombExploded;
    }

    public BombExploded getBombExploded() {
        return bombExploded;
    }
}

