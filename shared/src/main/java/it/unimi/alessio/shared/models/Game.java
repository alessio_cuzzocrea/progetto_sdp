package it.unimi.alessio.shared.models;

import it.unimi.alessio.shared.exceptions.ErrorCodes;
import it.unimi.alessio.shared.exceptions.GameException;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by alessio on 08/05/17.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Game {
    private String name;
    private int gridDimension;
    private int maxPoints;
    @XmlElement(required = false, name = "players")
    private List<Player> players = new ArrayList<Player>();

    public Game(){};

    public Game(String name, int gridDimension, int maxPoints, List<Player> players) {
        this.name = name;
        this.gridDimension = gridDimension;
        this.maxPoints = maxPoints;
        this.players=players;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGridDimension() {
        return gridDimension;
    }

    public void setGridDimension(int gridDimension) {
        this.gridDimension = gridDimension;
    }

    public int getMaxPoints() {
        return maxPoints;
    }

    public void setMaxPoints(int maxPoints) {
        this.maxPoints = maxPoints;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public void addUser(Player player) throws GameException {
        if(this.containsUser(player))
            throw new GameException(ErrorCodes.GameService.USERNAME_ALREADY_IN_USE);
        if(getUserCount() >= Math.pow(getGridDimension(), 2))
            throw new GameException(ErrorCodes.GameService.REACHED_MAX_PLAYERS);
        this.players.add(player);
    }

    public void removeUser(String userName) throws GameException{
        this.players.remove(new Player(userName));
    }

    public boolean containsUser(Player player) {
        return this.players.contains(player);
    }

    public int getUserCount() {
        return this.players.size();
    }


    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Game))
            return false;

        return ((Game) obj).name == this.name;
    }
}