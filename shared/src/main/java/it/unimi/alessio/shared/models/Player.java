package it.unimi.alessio.shared.models;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by alessio on 01/06/17.
 */
@XmlRootElement
public class Player {
    private String username;
    private String ip;
    private int port;

    public Player(String username, String ip, int port) {
        this.username = username;
        this.ip = ip;
        this.port = port;
    }
    public Player(){}

    public Player(String userName) {
        this.username = userName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Player){
            return ((Player) obj).getUsername().equals(this.username);
        }
        return false;
    }

    @Override
    public String toString() {
        return "Player{" +
                "username='" + username + '\'' +
                ", ip='" + ip + '\'' +
                ", port=" + port +
                '}';
    }
}
