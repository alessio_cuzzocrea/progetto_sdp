package it.unimi.alessio.clientsdp.models;

import it.unimi.alessio.shared.exceptions.ErrorCodes;
import it.unimi.alessio.shared.exceptions.GameException;
import it.unimi.alessio.shared.models.Game;
import it.unimi.alessio.shared.models.Player;

import java.util.List;

/**
 * Created by alessio on 27/06/17.
 */
public class SynchronizedGame extends Game {
    public SynchronizedGame(String name, int gridDimension, int maxPoints, List<Player> players) {
        super(name, gridDimension, maxPoints, players);
    }
    public synchronized void addPlayerAfterAnother(Player playerToAdd, Player basePlayer) {
        if(this.containsUser(playerToAdd))
            throw new GameException(ErrorCodes.GameService.USERNAME_ALREADY_IN_USE);

        if((double)this.getUserCount() >= Math.pow((double)this.getGridDimension(), 2.0D))
            throw new GameException(ErrorCodes.GameService.REACHED_MAX_PLAYERS);

        List<Player> players = getPlayers();
        int index = players.indexOf(basePlayer);
        players.add(index + 1, playerToAdd);
    }

    @Override
    public synchronized List<Player> getPlayers() {
        return super.getPlayers();
    }

    @Override
    public synchronized void setPlayers(List<Player> players) {
        super.setPlayers(players);
    }

    @Override
    public synchronized void addUser(Player player) throws GameException {
        super.addUser(player);
    }

    @Override
    public synchronized void removeUser(String userName) throws GameException {
        super.removeUser(userName);
    }

    @Override
    public synchronized int getUserCount() {
        return super.getUserCount();
    }

    public synchronized Player getNextPlayerAfter(Player player) {
        List<Player> list = getPlayers();
        int baseIndex = list.indexOf(player);
        int nextIndex = Math.floorMod(baseIndex + 1, list.size());
        return list.get(nextIndex);
    }
}
