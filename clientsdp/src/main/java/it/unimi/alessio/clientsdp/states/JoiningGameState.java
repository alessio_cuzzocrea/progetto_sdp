package it.unimi.alessio.clientsdp.states;

import com.google.gson.Gson;
import it.unimi.alessio.clientsdp.AppProperties;
import it.unimi.alessio.clientsdp.PropertiesFields;
import it.unimi.alessio.clientsdp.messages.*;
import it.unimi.alessio.clientsdp.models.Coordinates;
import it.unimi.alessio.clientsdp.storages.ResponseMessageStorage;
import it.unimi.alessio.clientsdp.threads.*;
import it.unimi.alessio.shared.models.Player;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by alessio on 09/06/17.
 */
public class JoiningGameState implements State {

    private Player getRandomPlayer(MatchContext matchContext) {
        Random random = new Random();
        boolean found = false;
        List<Player> list = matchContext.getCurrentGame().getPlayers();
        Player p = null;
        while (!found) {
            p = list.get(random.nextInt(list.size()));
            if (!p.equals(matchContext.getCurrentPlayer()))
                found = true;
        }
        boolean randomPlayer = Boolean.parseBoolean(AppProperties.getProperties().getProperty(PropertiesFields.connectToRandomPlayer));
        if(!randomPlayer)
            return list.get(0);
        else
            return p;
    }
    private List<Coordinates> getCoordinatesList(List<Message> responseList) {
        ArrayList<Coordinates> list = new ArrayList<>();
        for (Message response : responseList) {
            list.add(((MessageWithCoordinates) response).getCoordinates());
        }
        return list;
    }

    private void deployPlayer(MatchContext matchContext, List<Message> responseList) {
        List<Coordinates> coordinatesList = getCoordinatesList(responseList);
        Random random = new Random();
        boolean placed = false;
        Coordinates currentPosition = null;
        while (!placed) {
            int x = random.nextInt(matchContext.getCurrentGame().getGridDimension());
            int y = random.nextInt(matchContext.getCurrentGame().getGridDimension());
            currentPosition = new Coordinates(x, y, matchContext.getCurrentGame().getGridDimension());
            if (!coordinatesList.contains(currentPosition))
                placed = true;
        }
        matchContext.setCurrentPosition(currentPosition);
    }

    private boolean checkMessageType(String json, MessageType type){
        Message m = new Gson().fromJson(json, Message.class);
        return m.getType().equals(type);
    }

    @Override
    public void bootstrapState(MatchContext matchContext) {
        boolean joined = false;
        while (!joined) {
            Player playerToConnect = getRandomPlayer(matchContext);
            try {
                Socket predecessorSocket = new Socket(playerToConnect.getIp(), playerToConnect.getPort());
                Gson gson = new Gson();
                BufferedReader br = new BufferedReader(new InputStreamReader(predecessorSocket.getInputStream()));
                BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(predecessorSocket.getOutputStream()));

                Message announce = new MessageWithPlayer(
                        MessageType.ANNOUNCE_NEW_SUCCESSOR,
                        matchContext.getCurrentPlayer(),
                        playerToConnect,
                        matchContext.getCurrentPlayer()
                );
                bw.write(gson.toJson(announce) + "\n");
                bw.flush();
                String buffer;
                //aspetto il token
                do {
                    buffer = br.readLine();
                }while(!checkMessageType(buffer, MessageType.TOKEN_MESSAGE));
                System.out.println("token recv");


                //adesso posso chiedere la lista aggiornata dei giocatori
                Message requestFreshList = new Message(
                        MessageType.GET_FRESH_PLAYERS_LIST,
                        matchContext.getCurrentPlayer(),
                        playerToConnect
                );
                bw.write(gson.toJson(requestFreshList) + "\n");
                bw.flush();
                System.out.println("waiting for a fresh players list");
                do {
                    buffer = br.readLine();
                }while (!checkMessageType(buffer, MessageType.GET_FRESH_PLAYERS_LIST_RESPONSE));
                List<Player> players = gson.fromJson(buffer, MessageWithPlayerList.class).getPlayerList();
                matchContext.getCurrentGame().setPlayers(players);
                System.out.println("announcing to all players");
                for (Player p : matchContext.getCurrentGame().getPlayers()) {
                    if (!(p.equals(matchContext.getCurrentPlayer()) || p.equals(playerToConnect))) {
                        Socket socket = new Socket(p.getIp(), p.getPort());
                        new Thread(new SocketReaderThread(socket, matchContext)).start();
                        new Thread(new SocketWriterThread(socket, matchContext, p)).start();
                    }
                }
                new Thread(new SocketReaderThread(predecessorSocket, matchContext)).start();
                new Thread(new SocketWriterThread(predecessorSocket, matchContext, playerToConnect)).start();
                MessageWithPlayer m = new MessageWithPlayer(
                        MessageType.ANNOUNCE_NEW_PLAYER,
                        matchContext.getCurrentPlayer(),
                        null,
                        playerToConnect
                );

                matchContext.setResponseMessageStorage(new ResponseMessageStorage(matchContext.getCurrentGame().getUserCount() - 1));
                matchContext.getMessageStorage().putItem(m);
                List<Message> responseList = matchContext.getResponseMessageStorage().awaitResponses();
                System.out.println("tutti i giocatori sono stati avvisati");
                //adesso devo trovare una posizione non occupata
                deployPlayer(matchContext, responseList);

                joined = true;
            } catch (Exception e) {
                //se per qualche motivo cade la connessine devo eliminare il giocatore dalla lista e ripovare
                matchContext.getCurrentGame().removeUser(playerToConnect.getUsername());
                System.out.println("connection refused :(");
                if(matchContext.getCurrentGame().getUserCount() == 1){
                    System.out.println("non sono riuscito a farti connettere");
                    System.exit(1);
                }
            }
        }
        matchContext.setState(new InGameState());
        matchContext.bootstrapState();
        Message tokenMessage = new Message(
                MessageType.TOKEN_MESSAGE,
                matchContext.getCurrentPlayer(),
                matchContext.getNextPlayer()
        );
        matchContext.getMessageStorage().putItem(tokenMessage);
        System.out.println("joined");
    }
}
