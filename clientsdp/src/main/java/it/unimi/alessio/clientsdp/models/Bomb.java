package it.unimi.alessio.clientsdp.models;

/**
 * Created by alessio on 24/06/17.
 */
public class Bomb {
    private Area area;
    private int id;
    public Bomb(Area area, int id) {
        this.area = area;
        this.id = id;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Bomb{" +
                "area=" + area +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Bomb) {
            Bomb other = (Bomb) obj;
            return (this.area.equals(other.area) && (this.id == other.id));
        }
        return false;
    }
}
