package it.unimi.alessio.clientsdp.threads;

import it.unimi.alessio.clientsdp.messages.MessageType;
import it.unimi.alessio.clientsdp.messages.MessageWithBomb;
import it.unimi.alessio.clientsdp.messages.MessageWithBombExploded;
import it.unimi.alessio.clientsdp.models.Bomb;
import it.unimi.alessio.clientsdp.models.BombExploded;
import it.unimi.alessio.clientsdp.states.DeathState;
import it.unimi.alessio.clientsdp.states.MatchContext;

import java.util.Calendar;

/**
 * Created by alessio on 25/06/17.
 */
public class BombExplodedThread extends Thread {
    private Bomb bomb;
    private MatchContext matchContext;

    public BombExplodedThread(Bomb bomb, MatchContext matchContext) {
        this.bomb = bomb;
        this.matchContext = matchContext;
    }

    @Override
    public void run() {
        long timeToExplode = 5000;
        try {
            Thread.sleep(timeToExplode);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        boolean playerDied = false;
        System.out.println("la bomba per l'area" + this.bomb.getArea()+" è esplosa!");

        if(matchContext.getCurrentArea().equals(bomb.getArea()) && matchContext.setState(new DeathState())) {
            playerDied = true;
            System.out.println("sei morto per mano tua");
        }

        BombExploded bombExploded = new BombExploded(playerDied, bomb);
        matchContext.getBombExplodedList().add(bombExploded);
        MessageWithBombExploded  message = new MessageWithBombExploded(
                MessageType.BOMB_EXPLODED,
                matchContext.getCurrentPlayer(),
                null,
                bombExploded
        );
        matchContext.getMessageStorage().putItem(message);
    }
}
