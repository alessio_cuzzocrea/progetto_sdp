package it.unimi.alessio.clientsdp.messages.reactions;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import it.unimi.alessio.clientsdp.AppProperties;
import it.unimi.alessio.clientsdp.PropertiesFields;
import it.unimi.alessio.clientsdp.messages.Message;
import it.unimi.alessio.clientsdp.messages.MessageType;
import it.unimi.alessio.clientsdp.messages.MessageWithBomb;
import it.unimi.alessio.clientsdp.states.MatchContext;

import java.net.Socket;

/**
 * Created by alessio on 25/06/17.
 */
public class BombMessageReaction implements MessageReceivedReaction {
    @Override
    public void executeReaction(MatchContext matchContext, Socket socket, JsonObject jsonMessage) {
        MessageWithBomb receivedMessage = new Gson().fromJson(jsonMessage, MessageWithBomb.class);
        System.out.println("attenzione il giocatore " + receivedMessage.getSender().getUsername() +
                " ha appena lanciato una bomba nell'area: " + receivedMessage.getBomb().getArea());

        int sleepFactor = Integer.parseInt(AppProperties.getProperties().getProperty(PropertiesFields.sleepFactor));
        try {
            Thread.sleep(sleepFactor);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Message response = new Message(
                MessageType.BOMB_RESPONSE_MESSAGE,
                matchContext.getCurrentPlayer(),
                receivedMessage.getSender()
        );
        matchContext.getMessageStorage().putItem(response);
    }
}
