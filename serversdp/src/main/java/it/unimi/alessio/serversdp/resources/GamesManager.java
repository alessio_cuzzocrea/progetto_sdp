package it.unimi.alessio.serversdp.resources;


import it.unimi.alessio.shared.exceptions.ErrorCodes;
import it.unimi.alessio.shared.exceptions.GameException;
import it.unimi.alessio.shared.models.Game;
import it.unimi.alessio.shared.models.GameList;
import it.unimi.alessio.shared.models.Player;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Hashtable;

/**
 * Created by alessio on 11/05/17.
 */
//TODO: impostare il massimo numero di giocatori!
public class GamesManager {
    private static long sleepFactor = 0;
    private static GamesManager instance;
    private GameList gameListWrapper;


    private GamesManager() {
        this.gameListWrapper = new GameList();
        this.gameListWrapper.gameList = new Hashtable<String, Game>();
    }

    public synchronized static GamesManager getInstance() {

        try { Thread.sleep(sleepFactor);
        } catch (InterruptedException e) {e.printStackTrace();}

        if (instance == null)
            instance = new GamesManager();
        return instance;
    }

    public synchronized GameList getGamesList() {
        try { Thread.sleep(sleepFactor);
        } catch (InterruptedException e) {e.printStackTrace();}
        return deepCopy(this.gameListWrapper);
    }
    public synchronized void clearGamesList(){
        gameListWrapper.gameList = new Hashtable<>();
    }

    public synchronized Game addGame(Game g){
        try { Thread.sleep(sleepFactor);
        } catch (InterruptedException e) {e.printStackTrace();}

        if (this.gameListWrapper.gameList.containsKey(g.getName()))
            throw new GameException(ErrorCodes.GameService.GAME_NAME_ALREADY_IN_USE);
        this.gameListWrapper.gameList.put(g.getName(), g);
        return deepCopy(g);
    }

    public synchronized Game getGame(String gameName){
        Game g = this.gameListWrapper.gameList.get(gameName);
        if(g == null)
            throw new GameException(ErrorCodes.GameService.GAME_NOT_FOUND);
        return deepCopy(g);
    }

    public synchronized Game addUserToGame(String gameName, Player player) {
        try { Thread.sleep(sleepFactor);
        } catch (InterruptedException e) {e.printStackTrace();}

        if (!this.gameListWrapper.gameList.containsKey(gameName)) {
            throw new GameException(ErrorCodes.GameService.GAME_NOT_FOUND);
        }

        this.gameListWrapper.gameList.get(gameName).addUser(player);
        //deep copy of game
        Game g = deepCopy(this.gameListWrapper.gameList.get(gameName));
        return g;
    }


    public synchronized void removeUserFromGame(String gameName, String userName){
        try { Thread.sleep(sleepFactor);
        } catch (InterruptedException e) {e.printStackTrace();}

        if(!this.gameListWrapper.gameList.containsKey(gameName))
            throw new GameException(ErrorCodes.GameService.GAME_NOT_FOUND);
        this.gameListWrapper.gameList.get(gameName).removeUser(userName);
    }

    public synchronized void removeGame(String gameName) {
        try { Thread.sleep(sleepFactor);
        } catch (InterruptedException e) {e.printStackTrace();}
        Game g = this.gameListWrapper.gameList.get(gameName);
        if(g == null)
            throw new GameException(ErrorCodes.GameService.GAME_NOT_FOUND);
        this.gameListWrapper.gameList.remove(gameName);
    }

    public synchronized void removeGameWithoutUsers(String gameName) {
        try { Thread.sleep(sleepFactor);
        } catch (InterruptedException e) {e.printStackTrace();}

        Game g = this.gameListWrapper.gameList.get(gameName);
        if(g != null && g.getUserCount() == 0)
            this.gameListWrapper.gameList.remove(gameName);

    }

    private Game deepCopy(Game game) {
        Game g = null;
        try {
            JAXBContext ctx = JAXBContext.newInstance(Game.class);
            Marshaller jaxbMarshaller = ctx.createMarshaller();
            Unmarshaller jaxbUnmarshaller = ctx.createUnmarshaller();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            jaxbMarshaller.marshal(game, baos);
            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            g  = (Game) jaxbUnmarshaller.unmarshal(bais);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return g;
    }
    private GameList deepCopy(GameList gameList){
        GameList gl = null;
        try {
            JAXBContext ctx = JAXBContext.newInstance(GameList.class);
            Marshaller jaxbMarshaller = ctx.createMarshaller();
            Unmarshaller jaxbUnmarshaller = ctx.createUnmarshaller();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            jaxbMarshaller.marshal(gameList, baos);
            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            gl  = (GameList) jaxbUnmarshaller.unmarshal(bais);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return gl;
    }
}
