package it.unimi.alessio.clientsdp.models;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by alessio on 26/06/17.
 */
public class BombExploded {
    private final short MAX_POINTS_PER_BOMB = 3;
    private boolean currentPlayerDied;
    private Bomb bomb;
    private short pointsEarned;
    private final Object pointsLock = new Object();

    public BombExploded(boolean currentPlayerDied, Bomb bomb) {
        this.currentPlayerDied = currentPlayerDied;
        this.bomb = bomb;
        this.pointsEarned = 0;
    }

    public boolean incrementPointsEarned(){
        synchronized (pointsLock) {
            if (currentPlayerDied)
                return false;
            if (pointsEarned == MAX_POINTS_PER_BOMB)
                return false;

            pointsEarned++;
            return true;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof BombExploded){
            BombExploded other = (BombExploded) obj;
            if(other.bomb.equals(this.bomb))
                return true;
        }
        return false;
    }

    public Bomb getBomb() {
        return bomb;
    }
}
