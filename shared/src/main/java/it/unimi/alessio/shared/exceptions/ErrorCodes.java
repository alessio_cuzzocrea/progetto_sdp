package it.unimi.alessio.shared.exceptions;

/**
 * Created by alessio on 01/06/17.
 */
public class ErrorCodes {

    public enum GameService {
        USERNAME_ALREADY_IN_USE("nome utente già in uso"),
        GAME_NOT_FOUND("partita non trovata"),
        GAME_NAME_ALREADY_IN_USE("nome di gioco esistente"),
        REACHED_MAX_PLAYERS("numero massimio di giocatori raggiunto");
        private String description;

        GameService(String description) {
            this.description = description;
        }

        public String getDescription() {
            return description;
        }
    }
}
