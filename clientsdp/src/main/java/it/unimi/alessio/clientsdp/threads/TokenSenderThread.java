package it.unimi.alessio.clientsdp.threads;

import it.unimi.alessio.clientsdp.messages.Message;
import it.unimi.alessio.clientsdp.messages.MessageType;
import it.unimi.alessio.clientsdp.states.MatchContext;
import it.unimi.alessio.shared.models.Player;

/**
 * Created by alessio on 14/06/17.
 */
public class TokenSenderThread extends Thread {
    public MatchContext matchContext;

    public TokenSenderThread(MatchContext matchContext) {
        this.matchContext = matchContext;
    }

    @Override
    public void run() {
        Message tokenMessage = new Message(
                MessageType.TOKEN_MESSAGE,
                matchContext.getCurrentPlayer(),
                matchContext.getNextPlayer()
        );
        while (true) {
            matchContext.getTokenStorage().releaseToken();
            Player p = matchContext.getTmpNextPlayerQueue().getFirst();
            if(p != null)
                tokenMessage.setRecipient(p);
            else
                tokenMessage.setRecipient(matchContext.getNextPlayer());

            if(tokenMessage.getRecipient().equals(matchContext.getCurrentPlayer()))
                matchContext.getTokenStorage().putToken();
            else
                matchContext.getMessageStorage().putItem(tokenMessage);
        }
    }
}

