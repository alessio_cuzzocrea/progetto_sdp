package it.unimi.alessio.serversdp;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import java.util.logging.Level;

import it.unimi.alessio.shared.exceptions.GameException;
import org.glassfish.grizzly.utils.Exceptions;

import java.util.logging.Logger;

@Provider
public class CustomExceptionMapper implements
        ExceptionMapper<Exception> {
    @Override
    public Response toResponse(Exception ex) {
        if(ex instanceof GameException) {
            return Response.status(Response.Status.CONFLICT).entity(ex).type(MediaType.APPLICATION_JSON).build();
        }
        Logger.getLogger("server error").log(Level.SEVERE,ex.getMessage(), ex);
        return Response.status(500).entity(Exceptions.getStackTraceAsString(ex)).type("text/plain")
                .build();
    }
}