package it.unimi.alessio.clientsdp.storages;

import it.unimi.alessio.clientsdp.AppProperties;
import it.unimi.alessio.clientsdp.PropertiesFields;

import java.util.ArrayList;

/**
 * Created by alessio on 6/6/17.
 */
public class Storage<T> {
    protected T storedItem = null;
    protected final Object storedItemLock = new Object();
    protected Integer subscribers = 0;
    protected ArrayList<Long> servedThreads = new ArrayList<>();
    private int sleepFactor = Integer.parseInt(AppProperties.getProperties().getProperty(PropertiesFields.storageSleep));
    public void putItem(T item) {
        synchronized (storedItemLock) {
            try {
                Thread.sleep(sleepFactor);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (subscribers == 0)//do not store moves if there arent subscribers
                return;

            while (storedItem != null) {
                try {
                    storedItemLock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            storedItem = item;
            servedThreads.clear();
            storedItemLock.notifyAll();
        }
    }

    public T getItem() {

        synchronized (storedItemLock) {
            try {
                Thread.sleep(sleepFactor);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            waitForItem();
            T item = storedItem;
            servedThreads.add(Thread.currentThread().getId());
            checkSubscribers();
            return item;
        }
    }
    protected void waitForItem() {
        synchronized (storedItemLock) {
            while (storedItem == null || servedThreads.contains(Thread.currentThread().getId())) {
                //if there are no moves or i have already read this move then wait
                try {
                    storedItemLock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void subscribe() {
        synchronized (storedItemLock) {
            try {
                Thread.sleep(sleepFactor);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            subscribers++;
        }
    }

    public void unsubscribe() {
        synchronized (storedItemLock) {
            try {
                Thread.sleep(sleepFactor);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            servedThreads.remove(Thread.currentThread().getId());
            subscribers--;
            assert subscribers >= 0;
            checkSubscribers();
        }//quando il thread esce può capitare che  sia l'ultimo rimasto per leggere, e quindi il check subscribers == servedThreads.size non verrà più controllato.
    }

    private void checkSubscribers() {
        synchronized (storedItemLock) {
            try {
                Thread.sleep(sleepFactor);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (subscribers == servedThreads.size()) {
                storedItem = null;
                storedItemLock.notifyAll();
            }
        }
    }

    public boolean hasItem() {
        synchronized (storedItemLock){
            try {
                Thread.sleep(sleepFactor);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return  storedItem != null;
        }
    }

    public void waitToBeFree() {
        synchronized (storedItemLock){
            try {
                Thread.sleep(sleepFactor);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            while(hasItem()){
                try {
                    storedItemLock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public T peekItem() {
        synchronized (storedItemLock) {
            try {
                Thread.sleep(sleepFactor);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            waitForItem();
            return storedItem;
        }
    }
}
