package it.unimi.alessio.shared.models;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Hashtable;

/**
 * Created by alessio on 01/06/17.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GameList {
    @XmlElement(name = "games")
    public Hashtable<String, Game> gameList;
    public GameList(){}
}
