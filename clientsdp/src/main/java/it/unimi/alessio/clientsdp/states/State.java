package it.unimi.alessio.clientsdp.states;

import it.unimi.alessio.shared.models.Player;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

/**
 * Created by alessio on 09/06/17.
 */
public interface State {
    void bootstrapState(MatchContext matchContext);
}
