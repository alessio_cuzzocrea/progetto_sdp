package it.unimi.alessio.serversdp.services;

import it.unimi.alessio.shared.models.Game;
import it.unimi.alessio.serversdp.resources.GamesManager;
import it.unimi.alessio.shared.models.Player;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by alessio on 11/05/17.
 */
@Path("games")
public class GamesService {
    @Inject
    private javax.inject.Provider<org.glassfish.grizzly.http.server.Request> grizzlyRequest;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getGamesList(){
        return Response.ok(GamesManager.getInstance().getGamesList()).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{gameName}")
    public Response getGame(@PathParam("gameName") String gameName){
        Game g = GamesManager.getInstance().getGame(gameName);
        return Response.ok(g).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response createGame(Game g){
        //TODO: il lato della giriglia di gioco deve essere pari!
        Game createdGame = GamesManager.getInstance().addGame(g);
        return Response.ok(createdGame).build();
    }

    @PUT
    @Path("{gameName}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addUser(@PathParam("gameName") String gameName, Player player) {
        Game g = GamesManager.getInstance().addUserToGame(gameName, player);
        //TODO: controllare se la partita ha raggiunto il massimo dei giocatori
        return Response.ok(g).build();
    }
    @DELETE
    @Path("{gameName}")
    public Response removeGame(@PathParam("gameName") String gameName){
        GamesManager.getInstance().removeGame(gameName);
        return Response.ok().build();
    }
    @DELETE
    @Path("{gameName}/{userName}")
    public Response removeUser(@PathParam("gameName") String gameName, @PathParam("userName") String userName) {
        GamesManager.getInstance().removeUserFromGame(gameName,userName);
        //start a timer, if no one has entered in the game then delete it
        //set the timer only if i have deleted the last active player
        if(GamesManager.getInstance().getGame(gameName).getUserCount() == 0)
            GamesManager.getInstance().removeGame(gameName);
        return Response.ok().build();
    }
    @DELETE
    public Response clearGamesList(){
        GamesManager.getInstance().clearGamesList();
        return Response.ok().build();
    }
}
