package it.unimi.alessio.clientsdp.util;

import java.util.LinkedList;
import java.util.NoSuchElementException;

/**
 * Created by alessio on 21/06/17.
 */
public class SynchronizedQueue<T> {
    private LinkedList<T> queue;

    public  SynchronizedQueue(){
        this.queue = new LinkedList<>();
    }

    public synchronized void addLast(T item){
        this.queue.addLast(item);
    }
    public synchronized T getFirst(){
        try {
            return this.queue.removeFirst();
        }catch (NoSuchElementException e){
            return null;
        }
    }
    public synchronized T peekFirst(){
        return this.queue.peekFirst();
    }
    public synchronized void clear() {
        this.queue.clear();
    }
}
