package it.unimi.alessio.clientsdp.messages.reactions;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import it.unimi.alessio.clientsdp.AppProperties;
import it.unimi.alessio.clientsdp.PropertiesFields;
import it.unimi.alessio.clientsdp.messages.MessageWithPlayer;
import it.unimi.alessio.clientsdp.states.MatchContext;
import it.unimi.alessio.clientsdp.threads.SocketWriterThread;

import java.net.Socket;

/**
 * Created by alessio on 20/06/17.
 */
public class AnnounceNewSuccessorReaction implements MessageReceivedReaction {

    @Override
    public void executeReaction(MatchContext matchContext, Socket socket, JsonObject jsonMessage) {
        MessageWithPlayer message = new Gson().fromJson(jsonMessage, MessageWithPlayer.class);
        new Thread(new SocketWriterThread(socket, matchContext,message.getContainedPlayer())).start();
        matchContext.getTmpNextPlayerQueue().addLast(message.getContainedPlayer());

        boolean debugEnabled = Boolean.parseBoolean(AppProperties.getProperties().getProperty(PropertiesFields.debugMode));
        if(debugEnabled)
            System.out.println("new successor arrived");
    }
}
