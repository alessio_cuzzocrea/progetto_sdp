package it.unimi.alessio.clientsdp.threads;

import it.unimi.alessio.clientsdp.PlayingCommand;
import it.unimi.alessio.clientsdp.messages.Message;
import it.unimi.alessio.clientsdp.models.Bomb;
import it.unimi.alessio.clientsdp.states.MatchContext;
import it.unimi.alessio.clientsdp.storages.Storage;

import java.util.Scanner;

/**
 * Created by alessio on 22/05/17.
 */
public class UserInputThread extends Thread{
    private Storage<Message> messageStorage;
    private MatchContext matchContext;
    public UserInputThread(MatchContext matchContext){
        this.matchContext = matchContext;
        this.messageStorage = matchContext.getMessageStorage();
    }
    @Override
    public void run() {
        Scanner scan = new Scanner(System.in);
        int choice;
        while (true) {
            matchContext.getUserInputStorage().waitToBeFree();
            PlayingCommand.printCommandsMenu();
            System.out.print("Posizione attuale: " + matchContext.getCurrentPosition() + " area: " + matchContext.getCurrentPosition().getArea());
            System.out.println( " -- Punti: " + matchContext.getPoints());
            Bomb b = matchContext.getBombList().peekFirst();
            if(b != null)
                System.out.println("hai a dispozione una bomba " + b.getArea());
            try {
                choice = Integer.parseInt(scan.nextLine());
                if(PlayingCommand.valueOf(choice).equals(PlayingCommand.SUICIDE))
                    PlayingCommand.valueOf(choice).execute(matchContext);
                else
                    matchContext.getUserInputStorage().putItem(PlayingCommand.valueOf(choice));
            } catch (IllegalArgumentException e) {
                System.out.println("scelta non corrispondente a nessuna opzione");
            }
        }
    }
}
