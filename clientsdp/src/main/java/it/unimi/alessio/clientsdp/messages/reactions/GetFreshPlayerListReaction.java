package it.unimi.alessio.clientsdp.messages.reactions;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import it.unimi.alessio.clientsdp.messages.Message;
import it.unimi.alessio.clientsdp.messages.MessageType;
import it.unimi.alessio.clientsdp.messages.MessageWithPlayerList;
import it.unimi.alessio.clientsdp.states.MatchContext;

import java.net.Socket;

/**
 * Created by alessio on 20/06/17.
 */
public class GetFreshPlayerListReaction implements MessageReceivedReaction {
    @Override
    public void executeReaction(MatchContext matchContext, Socket socket, JsonObject jsonMessage) {
        Message m = new Gson().fromJson(jsonMessage, Message.class);
        matchContext.getCurrentGame().addPlayerAfterAnother(m.getSender(), matchContext.getCurrentPlayer());
        MessageWithPlayerList responseMessage = new MessageWithPlayerList(
                MessageType.GET_FRESH_PLAYERS_LIST_RESPONSE,
                matchContext.getCurrentPlayer(),
                m.getSender(),
                matchContext.getCurrentGame().getPlayers()
        );
        matchContext.getMessageStorage().putItem(responseMessage);
    }
}
