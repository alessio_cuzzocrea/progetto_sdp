package it.unimi.alessio.clientsdp.states;

import it.unimi.alessio.clientsdp.AppProperties;
import it.unimi.alessio.clientsdp.PlayingCommand;
import it.unimi.alessio.clientsdp.PropertiesFields;
import it.unimi.alessio.clientsdp.RestClient;
import it.unimi.alessio.clientsdp.messages.Message;
import it.unimi.alessio.clientsdp.messages.MessageType;
import it.unimi.alessio.clientsdp.messages.reactions.*;
import it.unimi.alessio.clientsdp.models.*;
import it.unimi.alessio.clientsdp.storages.ResponseMessageStorage;
import it.unimi.alessio.clientsdp.storages.Storage;
import it.unimi.alessio.clientsdp.storages.TokenStorage;
import it.unimi.alessio.clientsdp.threads.SocketReaderThread;
import it.unimi.alessio.clientsdp.threads.SocketWriterThread;
import it.unimi.alessio.clientsdp.util.ConcreteBuffer;
import it.unimi.alessio.clientsdp.util.SynchronizedList;
import it.unimi.alessio.clientsdp.util.SynchronizedQueue;
import it.unimi.alessio.shared.models.Player;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.Hashtable;

/**
 * Created by alessio on 09/06/17.
 */
public class MatchContext {
    private State state;
    private SynchronizedGame currentGame;
    private Coordinates currentPosition;
    private Player currentPlayer;
    private volatile ResponseMessageStorage responseMessageStorage;
    private int points;
    private RestClient restClient;
    //FINAL FIELDS
    private final SynchronizedList<BombExploded> bombExplodedList = new SynchronizedList<BombExploded>();
    private final ServerSocket serverSocket = new ServerSocket(0);
    private final SynchronizedQueue<Player> tmpNextPlayerQueue = new SynchronizedQueue<Player>();
    private final SynchronizedList<SocketWriterThread> socketWriterThreadList = new SynchronizedList<>();
    private final SynchronizedList<SocketReaderThread> socketReaderThreadList = new SynchronizedList<>();
    private final Hashtable<MessageType, MessageReceivedReaction> reactionTable = new Hashtable<>();
    private final ConcreteBuffer measurementList = new ConcreteBuffer();
    private final Storage<PlayingCommand> userInputStorage = new Storage<PlayingCommand>();
    private final TokenStorage tokenStorage = new TokenStorage();
    private final Storage<Message> messageStorage = new Storage<Message>();
    private final SynchronizedQueue<Bomb> bombList = new SynchronizedQueue<Bomb>();
    //LOCKS
    private final Object stateLock = new Object();
    private final Object pointsLock = new Object();
    private final Object coordinatesLock = new Object();

    public MatchContext() throws IOException {
        state = null;
        points = 0;

        reactionTable.put(MessageType.ANNOUNCE_NEW_SUCCESSOR, new AnnounceNewSuccessorReaction());
        reactionTable.put(MessageType.GET_FRESH_PLAYERS_LIST, new GetFreshPlayerListReaction());
        reactionTable.put(MessageType.TOKEN_MESSAGE, new TokenMessageReaction());
        reactionTable.put(MessageType.ANNOUNCE_NEW_PLAYER, new AnnounceNewPlayerReaction());
        reactionTable.put(MessageType.ANNOUNCE_NEW_PLAYER_RESPONSE, new AnnounceNewPlayerResponseReaction());
        reactionTable.put(MessageType.MOVE_MESSAGE, new MoveMessageReaction());
        reactionTable.put(MessageType.MOVE_RESPONSE_DIED, new MoveMessageResponseReaction());
        reactionTable.put(MessageType.MOVE_RESPONSE_NOT_DIED, new MoveMessageResponseReaction());
        reactionTable.put(MessageType.ANNOUNCE_PLAYER_DEATH, new AnnounceDeathReaction());
        reactionTable.put(MessageType.ANNOUNCE_VICTORY, new AnnounceVictoryReaction());
        reactionTable.put(MessageType.BOMB_MESSAGE, new BombMessageReaction());
        reactionTable.put(MessageType.BOMB_RESPONSE_MESSAGE, new BombResponseReaction());
        reactionTable.put(MessageType.BOMB_EXPLODED, new BombExplodedReaction());
        reactionTable.put(MessageType.BOMB_DEATH_OCCURRED, new BombDeathReaction());
        reactionTable.put(MessageType.DEATH_ACKNOWLEDGE, new DeathAcknowledgeReaction());
    }

    public boolean setState(State state){
        synchronized (stateLock) {
            if(state != null && state.getClass().equals(DeathState.class)){
                int sleepFactor = Integer.parseInt(AppProperties.getProperties().getProperty(PropertiesFields.sleepBeforeDie));
                try {
                    Thread.sleep(sleepFactor);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if(this.state != null && this.state.getClass().equals(state.getClass())){
                return false;
            }
            this.state = state;
            return true;
        }
    }

    public State getState(){
        synchronized (stateLock) {
            return state;
        }
    }

    public ServerSocket getServerSocket() {
        return serverSocket;
    }

    public Storage<Message> getMessageStorage() {
        return messageStorage;
    }

    public SynchronizedGame getCurrentGame() {
        return currentGame;
    }

    public void setCurrentGame(SynchronizedGame currentGame) {
        this.currentGame = currentGame;
    }

    public Coordinates getCurrentPosition() {
        synchronized (coordinatesLock) {
            return currentPosition;
        }
    }

    public void setCurrentPosition(Coordinates currentPosition) {
        synchronized (coordinatesLock) {
            this.currentPosition = currentPosition;
        }
    }

    public Area getCurrentArea() {
        synchronized (coordinatesLock) {
            return currentPosition.getArea();
        }
    }

    public void doMove(PlayingCommand move){
        synchronized (coordinatesLock){
            switch (move){
                case GO_UP:
                    currentPosition.decrementY();
                    break;
                case GO_DOWN:
                    currentPosition.incremetY();
                    break;
                case GO_LEFT:
                    currentPosition.decrementX();
                    break;
                case GO_RIGHT:
                    currentPosition.incrementX();
                    break;
            }
        }
    }

    public Player getNextPlayer() {
        return getCurrentGame().getNextPlayerAfter(getCurrentPlayer());
    }

    public TokenStorage getTokenStorage() {
        return tokenStorage;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public void setCurrentPlayer(Player currentPlayer) {
        this.currentPlayer = currentPlayer;
    }

    public Storage<PlayingCommand> getUserInputStorage() {
        return userInputStorage;
    }

    public SynchronizedList<SocketReaderThread> getSocketReaderThreadList() {
        return socketReaderThreadList;
    }

    public SynchronizedList<SocketWriterThread> getSocketWriterThreadList() {
        return socketWriterThreadList;
    }

    public Hashtable<MessageType, MessageReceivedReaction> getReactionTable() {
        return reactionTable;
    }

    public ResponseMessageStorage getResponseMessageStorage() {
        return responseMessageStorage;
    }

    public void setResponseMessageStorage(ResponseMessageStorage responseMessageStorage) {
        this.responseMessageStorage = responseMessageStorage;
    }

    public ConcreteBuffer getMeasurementList() {
        return measurementList;
    }

    public int getPoints() {
        synchronized (pointsLock) {
            return points;
        }
    }

    public void incrementPoints() {
        synchronized (pointsLock) {
            points++;
        }
    }

    public boolean hasReachedMaxPoints(){
        synchronized (pointsLock){
            return points >= currentGame.getMaxPoints();
        }
    }

    public SynchronizedQueue<Player> getTmpNextPlayerQueue() {
        return tmpNextPlayerQueue;
    }

    public SynchronizedList<BombExploded> getBombExplodedList() {
        return bombExplodedList;
    }

    public SynchronizedQueue<Bomb> getBombList() {
        return bombList;
    }

    public void checkVictory() {
        if (hasReachedMaxPoints()) {
            Message announceVictory = new Message(
                    MessageType.ANNOUNCE_VICTORY,
                    getCurrentPlayer(),
                    null
            );
            messageStorage.putItem(announceVictory);
            try {
                restClient.deleteGame(getCurrentGame());
                System.out.println("hai vinto!");
            }catch (RuntimeException e){
                System.out.println("partita già vinta da qualcun altro");
            }
            System.out.println("punteggio finale: " + getPoints());
            messageStorage.waitToBeFree();
            System.exit(1);
        }
    }

    public RestClient getRestClient() {
        return restClient;
    }

    public void setRestClient(RestClient restClient) {
        this.restClient = restClient;
    }
    //----------------STATE METHODS

    public void bootstrapState(){
        this.state.bootstrapState(this);
    }



}
