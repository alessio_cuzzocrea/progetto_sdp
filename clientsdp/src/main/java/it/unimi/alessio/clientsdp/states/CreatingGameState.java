package it.unimi.alessio.clientsdp.states;

import it.unimi.alessio.clientsdp.models.Coordinates;
import it.unimi.alessio.clientsdp.threads.ServerThread;
import it.unimi.alessio.clientsdp.threads.TokenConsumer;
import it.unimi.alessio.clientsdp.threads.TokenSenderThread;
import it.unimi.alessio.clientsdp.threads.UserInputThread;
import it.unimi.alessio.shared.models.Player;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.util.Random;

/**
 * Created by alessio on 09/06/17.
 */
public class CreatingGameState implements State {

    @Override
    public void bootstrapState(MatchContext matchContext) {
        Random r = new Random();
        int gridDimension = matchContext.getCurrentGame().getGridDimension();
        int x = r.nextInt(gridDimension);
        int y = r.nextInt(gridDimension);
        matchContext.setCurrentPosition(new Coordinates(x,y, gridDimension));
        matchContext.setState(new InGameState());
        matchContext.getTokenStorage().putToken();
        matchContext.bootstrapState();
    }
}
