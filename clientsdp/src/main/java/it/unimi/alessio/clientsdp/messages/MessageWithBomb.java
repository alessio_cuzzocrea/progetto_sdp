package it.unimi.alessio.clientsdp.messages;

import it.unimi.alessio.clientsdp.models.Bomb;
import it.unimi.alessio.shared.models.Player;

/**
 * Created by alessio on 25/06/17.
 */
public class MessageWithBomb extends Message {
    private Bomb bomb;

    public MessageWithBomb(MessageType type, Player sender, Player recipient, Bomb bomb) {
        super(type, sender, recipient);
        this.bomb = bomb;
    }

    public Bomb getBomb() {
        return bomb;
    }

    public void setBomb(Bomb bomb) {
        this.bomb = bomb;
    }
}
