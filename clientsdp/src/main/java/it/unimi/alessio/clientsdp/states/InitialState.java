package it.unimi.alessio.clientsdp.states;

import it.unimi.alessio.clientsdp.AppProperties;
import it.unimi.alessio.clientsdp.MainMenuCommand;
import it.unimi.alessio.clientsdp.PropertiesFields;
import it.unimi.alessio.clientsdp.RestClient;
import it.unimi.alessio.clientsdp.messages.Message;
import it.unimi.alessio.clientsdp.storages.Storage;
import it.unimi.alessio.shared.models.Player;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 * Created by alessio on 11/06/17.
 */
public class InitialState implements State {
    @Override
    public void bootstrapState(MatchContext matchContext) {
        String username, gameName, serverIp, serverPort;
        boolean debugMode = Boolean.parseBoolean(AppProperties.getProperties().getProperty(PropertiesFields.debugMode));
        Player currentPlayer;
        Scanner scan = new Scanner(System.in);

        System.out.println("inserisci il nome utente");
        username = scan.nextLine();
        if(username.isEmpty()) {
            username = "alessio" + matchContext.getServerSocket().getLocalPort();
            System.out.println(username);
        }
        String hostname;
        try {
             hostname = InetAddress.getLocalHost().getCanonicalHostName();
        } catch (UnknownHostException e) {
            hostname = "localhost";
        }
        currentPlayer = new Player(username, hostname, matchContext.getServerSocket().getLocalPort());
        matchContext.setCurrentPlayer(currentPlayer);
        System.out.println("inserisci l'indirizzo ip del server");
        serverIp = scan.nextLine();
        if(serverIp.isEmpty())
            serverIp = "localhost";
        System.out.println("inserisci la porta del server");
        serverPort = scan.nextLine();
        if(serverPort.isEmpty())
            serverPort = "8080";
        matchContext.setRestClient(new RestClient(serverIp, serverPort));
    }
}
