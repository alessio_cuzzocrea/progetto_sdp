package it.unimi.alessio.clientsdp.messages;

import it.unimi.alessio.shared.models.Player;

/**
 * Created by alessio on 05/06/17.
 */
public class Message {
    protected MessageType type;
    protected Player sender;
    protected Player recipient = null;//if the recipient is null, then is a broadcast message

    public Message(MessageType type, Player sender, Player recipient) {
        this.type = type;
        this.recipient = recipient;
        this.sender = sender;
    }

    public Message() {
    }

    public boolean isDestinedTo(Player player){
        if(recipient == null || recipient.equals(player))
            return true;
        return false;
    }

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    public Player getRecipient() {
        return recipient;
    }

    public void setRecipient(Player recipient) {
        this.recipient = recipient;
    }

    public Player getSender() {
        return sender;
    }

    public void setSender(Player sender) {
        this.sender = sender;
    }

    @Override
    public String toString() {
        return "Message{" +
                "type=" + type +
                ", recipient=" + recipient +
                '}';
    }
}
