package it.unimi.alessio.clientsdp.messages.reactions;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import it.unimi.alessio.clientsdp.messages.Message;
import it.unimi.alessio.clientsdp.states.MatchContext;

import java.net.Socket;

/**
 * Created by alessio on 25/06/17.
 */
public class BombResponseReaction implements MessageReceivedReaction {

    @Override
    public void executeReaction(MatchContext matchContext, Socket socket, JsonObject jsonMessage) {
        Message message = new Gson().fromJson(jsonMessage, Message.class);
        matchContext.getResponseMessageStorage().putResponse(message);
    }
}
