package it.unimi.alessio.clientsdp;

/**
 * Created by alessio on 24/06/17.
 */
public class PropertiesFields {
    public static final String sleepFactor = "sleepFactor";
    public static final String debugMode = "debugMode";
    public static final String printConsumingToken = "printConsumingToken";
    public static final String askForGameSpecs = "askForGameSpecs";
    public static final String sleepBeforeDie = "sleepBeforDie";
    public static final String connectToRandomPlayer = "connectToRandomPlayer";
    public static final String addBombs = "addBombs";
    public static final String storageSleep = "storageSleep";
    public static final String responseStorageSleep = "responseStorageSleep";
}
