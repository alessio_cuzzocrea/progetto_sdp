package it.unimi.alessio.serversdp;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import it.unimi.alessio.serversdp.resources.GamesManager;
import org.eclipse.persistence.jaxb.rs.MOXyJsonProvider;
import org.glassfish.grizzly.http.server.HttpServer;

import org.glassfish.jersey.client.ClientConfig;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GameServiceTest {

    private HttpServer server;
    private WebTarget target;

    @Before
    public void setUp() throws Exception {
        // start the server
        server = Main.startServer();
        // create the client
        Client c = ClientBuilder.newClient();
        ClientConfig cc = new ClientConfig();
        //che differenza c'è???
        cc.register(MOXyJsonProvider.class);
//        cc.register(Logging)
        // uncomment the following line if you want to enable
        // support for JSON in the client (you also have to uncomment
        // dependency on jersey-media-json module in pom.xml and Main.startServer())
        // --
//         c.configuration().enable(new org.glassfish.jersey.media.json.JsonJaxbFeature());
//         c.register()
        target = c.target(Main.BASE_URI);
    }

    @After
    public void tearDown() throws Exception {
        server.stop();
    }

    /**
     * Test to see that the message "Got it!" is sent in the response.
     */
    @Test
    public void testGetWihtoutGames() {
        GamesManager h = target.path("games").request().get(GamesManager.class);


        assertEquals(0, h.getGamesList().size());
    }
}
