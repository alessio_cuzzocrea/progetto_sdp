package it.unimi.alessio.clientsdp;

import it.unimi.alessio.clientsdp.models.SynchronizedGame;
import it.unimi.alessio.clientsdp.states.CreatingGameState;
import it.unimi.alessio.clientsdp.states.JoiningGameState;
import it.unimi.alessio.clientsdp.states.MatchContext;
import it.unimi.alessio.shared.models.Game;

import java.util.Scanner;

/**
 * Created by alessio on 08/06/17.
 */
public enum MainMenuCommand {
    GET_GAMES("lista partite") {
        @Override
        public void execute(Scanner scan, MatchContext matchContext) {
            matchContext.getRestClient().getGames();
        }
    },
    VIEW_GAME("visualizza dettagli partita") {
        @Override
        public void execute(Scanner scan, MatchContext matchContext) {
            System.out.println("inserisci il nome partita");
            String gameName = scan.nextLine();
            matchContext.getRestClient().getGame(gameName);
        }
    },
    CREATE_GAME("crea partita") {
        @Override
        public void execute(Scanner scan, MatchContext matchContext) {
            boolean askForGameSpecs = Boolean.parseBoolean(AppProperties.getProperties().getProperty(PropertiesFields.askForGameSpecs));
            System.out.println("nome partita?");
            String gameName = scan.nextLine();
            System.out.println("grandezza di un lato della griglia? (numero pari e maggiore di 3)");
            int gridDimension = 8;
            if(askForGameSpecs)
                do {
                    try {
                        gridDimension = Integer.parseInt(scan.nextLine());

                        if (gridDimension % 2 != 0 || gridDimension < 4)
                            System.out.println("riprova");
                    }catch (NumberFormatException e){
                        System.out.println("inserisci un numero");
                    }
                }while(gridDimension % 2 != 0 || gridDimension < 4);
            System.out.println("punteggio massimo?");
            int maxPoints = 2;
            if(askForGameSpecs) {
                boolean exit = false;
                while (!exit)
                    try {
                        maxPoints = Integer.parseInt(scan.nextLine());
                        if(maxPoints > 0)
                            exit = true;
                        else
                            System.out.println("il punteggio deve essere maggiore di 0");
                    } catch (NumberFormatException e) {
                        System.out.println("inserisci un numero");
                    }
            }
            Game g = matchContext.getRestClient().createGame(gameName, gridDimension, maxPoints,matchContext.getCurrentPlayer() );
            if (g != null) {
                SynchronizedGame sg = new SynchronizedGame(
                        g.getName(),
                        g.getGridDimension(),
                        g.getMaxPoints(),
                        g.getPlayers()
                );
                matchContext.setCurrentGame(sg);
                CreatingGameState createdGame = new CreatingGameState();
                matchContext.setState(createdGame);
            }
        }
    },
    JOIN_GAME("unisciti ad una partita") {
        @Override
        public void execute(Scanner scan, MatchContext matchContext) {
            System.out.println("nome partita?");
            String gameName = scan.nextLine();
            Game g = matchContext.getRestClient().putPlayerToGame(gameName, matchContext.getCurrentPlayer());
            if(g!= null){
                SynchronizedGame sg = new SynchronizedGame(
                        g.getName(),
                        g.getGridDimension(),
                        g.getMaxPoints(),
                        g.getPlayers()
                );
                matchContext.setCurrentGame(sg);
                JoiningGameState state = new JoiningGameState();
                matchContext.setState(state);
            }
        }
    };

    private String description;

    MainMenuCommand(String description) {
        this.description = description;
    }

    public static MainMenuCommand valueOf(int ordinal) {
        for (MainMenuCommand m : MainMenuCommand.values()) {
            if (m.ordinal() == ordinal)
                return m;
        }
        throw new IllegalArgumentException();
    }

    public String getDescription() {
        return description;
    }

    public static void printCommandsMenu() {
        System.out.println("Cosa vuoi fare?");
        for (MainMenuCommand command :
                MainMenuCommand.values()) {
            System.out.println(command.ordinal() + " - " + command.getDescription());
        }
    }

    public abstract void execute(Scanner scan, MatchContext matchContext);
}

