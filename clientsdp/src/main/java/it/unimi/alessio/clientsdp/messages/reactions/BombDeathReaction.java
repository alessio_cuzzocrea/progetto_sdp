package it.unimi.alessio.clientsdp.messages.reactions;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import it.unimi.alessio.clientsdp.RestClient;
import it.unimi.alessio.clientsdp.messages.Message;
import it.unimi.alessio.clientsdp.messages.MessageType;
import it.unimi.alessio.clientsdp.messages.MessageWithBombExploded;
import it.unimi.alessio.clientsdp.models.BombExploded;
import it.unimi.alessio.clientsdp.states.MatchContext;

import java.net.Socket;

/**
 * Created by alessio on 26/06/17.
 */
public class BombDeathReaction implements MessageReceivedReaction {
    @Override
    public void executeReaction(MatchContext matchContext, Socket socket, JsonObject jsonMessage) {
        MessageWithBombExploded message = new Gson().fromJson(jsonMessage, MessageWithBombExploded.class);
        BombExploded bomb = matchContext.getBombExplodedList().get(message.getBombExploded());
        if(bomb.incrementPointsEarned()) {
            matchContext.incrementPoints();
            matchContext.checkVictory();
        }
    }
}
