package it.unimi.alessio.clientsdp.threads;

import it.unimi.alessio.clientsdp.AppProperties;
import it.unimi.alessio.clientsdp.PropertiesFields;
import it.unimi.alessio.clientsdp.models.Area;
import it.unimi.alessio.clientsdp.models.Bomb;
import it.unimi.alessio.clientsdp.util.SynchronizedQueue;
import it.unimi.alessio.libsdp.simulator.Buffer;
import it.unimi.alessio.libsdp.simulator.Measurement;

import java.util.List;

/**
 * Created by alessio on 24/06/17.
 */
public class BombGenerator extends Thread{
    private Buffer<Measurement> measurementBuffer;
    private SynchronizedQueue<Bomb> bombQueue;
    private Double ema;
    private Double alpha;
    private Double threshold;
    private int bombCounter;
    public BombGenerator(Buffer<Measurement> measurementBuffer, SynchronizedQueue<Bomb> bombQueue) {
        this.measurementBuffer = measurementBuffer;
        this.ema = null;
        this.alpha = 0.8d;
        this.threshold = 40d;
        this.bombQueue = bombQueue;
        bombCounter = 0;
    }

    @Override
    public void run() {
        double mean;
        boolean addBombs = Boolean.parseBoolean(AppProperties.getProperties().getProperty(PropertiesFields.addBombs));
        if(addBombs) {
            bombQueue.addLast(new Bomb(Area.GIALLA, bombCounter));
            bombCounter++;
            bombQueue.addLast(new Bomb(Area.BLUE, bombCounter));
            bombCounter++;
            bombQueue.addLast(new Bomb(Area.VERDE, bombCounter));
            bombCounter++;
            bombQueue.addLast(new Bomb(Area.ROSSA, bombCounter));
            bombCounter++;
        }
        while (true){
            mean = 0;
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            List<Measurement> l = measurementBuffer.readAllAndClean();
            for (Measurement m:l) {
                mean+= m.getValue();
            }
            mean = (mean / l.size());
            Double newEma;
            if(ema != null)
                newEma  = ema + (alpha * (mean - ema));
            else {
                newEma = ema = mean;
            }
            if(newEma - ema > threshold) {
                int ceiledEma = new Double(Math.ceil(newEma)).intValue();
                Bomb bomb = new Bomb(Area.valueOf(Math.floorMod(ceiledEma, 4)), bombCounter);
                bombCounter++;
                bombQueue.addLast(bomb);
                System.out.println("aggiunta una bomba " + bomb.getArea());
            }
            ema=newEma;
        }
    }
}
