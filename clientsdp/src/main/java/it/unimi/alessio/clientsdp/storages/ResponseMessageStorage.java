package it.unimi.alessio.clientsdp.storages;

import it.unimi.alessio.clientsdp.AppProperties;
import it.unimi.alessio.clientsdp.PropertiesFields;
import it.unimi.alessio.clientsdp.messages.Message;

import java.util.ArrayList;

/**
 * Created by alessio on 6/6/17.
 */
//in this case we have multiple writers and a single consumer
    //ideally the consumer reads the messages and do the operations needed
public class ResponseMessageStorage {
    private final ArrayList<Message> messageQueue = new ArrayList<Message>();
    private int expectedResponses = 0;
    private final Object lock = new Object();
    private int sleepFactor = Integer.parseInt(AppProperties.getProperties().getProperty(PropertiesFields.responseStorageSleep));
    public ResponseMessageStorage(int expectedResponses){
        this.expectedResponses = expectedResponses;
    }

    public void putResponse(Message message){
        synchronized (lock){
            try {
                Thread.sleep(sleepFactor);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            this.messageQueue.add(message);
            lock.notifyAll();
        }
    }
    public ArrayList<Message> awaitResponses()  {
        synchronized (lock){
            try {
                Thread.sleep(sleepFactor);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            while (messageQueue.size() < expectedResponses){
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return messageQueue;
        }
    }

}
