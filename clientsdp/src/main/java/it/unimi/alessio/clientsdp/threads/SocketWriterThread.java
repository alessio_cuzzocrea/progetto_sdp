package it.unimi.alessio.clientsdp.threads;

import com.google.gson.Gson;

import it.unimi.alessio.clientsdp.AppProperties;
import it.unimi.alessio.clientsdp.PropertiesFields;
import it.unimi.alessio.clientsdp.messages.Message;
import it.unimi.alessio.clientsdp.messages.MessageType;
import it.unimi.alessio.clientsdp.states.MatchContext;
import it.unimi.alessio.clientsdp.storages.Storage;
import it.unimi.alessio.shared.models.Player;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;

/**
 * Created by alessio on 04/06/17.
 */
public class SocketWriterThread extends Thread {
    private Socket socket;
    private BufferedWriter outputData;
    private Storage<Message> messageStorage;
    private Player playerBound;
    private MatchContext matchContext;

    public SocketWriterThread(Socket acceptSocket, MatchContext matchContext, Player playerBound) {
        this.socket = acceptSocket;
        this.messageStorage = matchContext.getMessageStorage();
        this.playerBound = playerBound;
        this.matchContext = matchContext;
        messageStorage.subscribe();
//        TokenStorage.subscribe();
        try {
            this.outputData = new BufferedWriter(new OutputStreamWriter(acceptSocket.getOutputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.matchContext.getSocketWriterThreadList().add(this);
    }

    public boolean isBoundToPlayer(Player player) {
        return player.equals(playerBound);
    }

    public void sendMessage(Message m) {
        try {
            outputData.write(new Gson().toJson(m) + "\n");
            outputData.flush();
        } catch (IOException e) {//socket closed
            messageStorage.unsubscribe();
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        boolean exit = false;
        while (!exit) {
            try {
                Message m = messageStorage.peekItem();
                if (m.isDestinedTo(playerBound)) {
                    outputData.write(new Gson().toJson(m) + "\n");
                    outputData.flush();
                }
                messageStorage.getItem();
            } catch (IOException e) {
                exit = true;
                messageStorage.unsubscribe();
            }
        }
        this.matchContext.getSocketWriterThreadList().remove(this);
    }

    public void closeSocket() {
        try {
            this.socket.close();
        } catch (IOException e) {
            boolean debugEnabled = Boolean.parseBoolean(AppProperties.getProperties().getProperty(PropertiesFields.debugMode));
            if(debugEnabled)
                System.out.println("socket writer: socket closed already");
        }
    }
}