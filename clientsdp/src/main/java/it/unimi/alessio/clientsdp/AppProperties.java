package it.unimi.alessio.clientsdp;

import java.util.Properties;

/**
 * Created by alessio on 24/06/17.
 */
public class AppProperties {
    private static final Properties properties = new Properties();

    public static Properties getProperties() {
        return properties;
    }

}
