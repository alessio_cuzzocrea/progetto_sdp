package it.unimi.alessio.clientsdp.threads;

import it.unimi.alessio.clientsdp.AppProperties;
import it.unimi.alessio.clientsdp.PropertiesFields;
import it.unimi.alessio.clientsdp.states.MatchContext;

import java.io.IOException;
import java.net.Socket;

/**
 * Created by alessio on 04/06/17.
 */
public class ServerThread extends Thread {

    private MatchContext matchContext;
    public ServerThread(MatchContext matchContext){
        this.matchContext = matchContext;
    }

    @Override
    public void run() {
            try {
                while(true){
                    boolean debugEnabled = Boolean.parseBoolean(AppProperties.getProperties().getProperty(PropertiesFields.debugMode));
                    if(debugEnabled)
                        System.out.println("waiting for a connetion");
                    Socket acceptSocket = matchContext.getServerSocket().accept();
                    new SocketReaderThread(acceptSocket, matchContext).start();
                }
            }catch (IOException e) {
                System.out.println("server closed");
            }
    }
}
