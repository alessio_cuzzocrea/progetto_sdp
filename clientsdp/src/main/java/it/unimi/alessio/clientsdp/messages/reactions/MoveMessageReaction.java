package it.unimi.alessio.clientsdp.messages.reactions;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import it.unimi.alessio.clientsdp.messages.Message;
import it.unimi.alessio.clientsdp.messages.MessageType;
import it.unimi.alessio.clientsdp.messages.MessageWithCoordinates;
import it.unimi.alessio.clientsdp.states.DeathState;
import it.unimi.alessio.clientsdp.states.MatchContext;

import java.net.Socket;

/**
 * Created by alessio on 22/06/17.
 */
public class MoveMessageReaction implements MessageReceivedReaction {
    @Override
    public void executeReaction(MatchContext matchContext, Socket socket, JsonObject jsonMessage) {
        MessageWithCoordinates message = new Gson().fromJson(jsonMessage, MessageWithCoordinates.class);
        Message response;

        if (message.getCoordinates().equals(matchContext.getCurrentPosition()) &&
                matchContext.setState(new DeathState())){
            response = new Message(
                    MessageType.MOVE_RESPONSE_DIED,
                    matchContext.getCurrentPlayer(),
                    message.getSender()
                    );
            System.out.println("sei morto per mano di: " + message.getSender().getUsername());
        } else {
            response = new Message(
                    MessageType.MOVE_RESPONSE_NOT_DIED,
                    matchContext.getCurrentPlayer(),
                    message.getSender()
            );
        }
        matchContext.getMessageStorage().putItem(response);
    }
}

