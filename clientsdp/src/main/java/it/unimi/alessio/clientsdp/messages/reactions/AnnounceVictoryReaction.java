package it.unimi.alessio.clientsdp.messages.reactions;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import it.unimi.alessio.clientsdp.messages.Message;
import it.unimi.alessio.clientsdp.states.MatchContext;

import java.net.Socket;

/**
 * Created by alessio on 23/06/17.
 */
public class AnnounceVictoryReaction implements MessageReceivedReaction {
    @Override
    public void executeReaction(MatchContext matchContext, Socket socket, JsonObject jsonMessage) {
        Message m = new Gson().fromJson(jsonMessage, Message.class);
        System.out.println("la partita è stata vinta da: " + m.getSender().getUsername());
        System.exit(1);
    }
}
