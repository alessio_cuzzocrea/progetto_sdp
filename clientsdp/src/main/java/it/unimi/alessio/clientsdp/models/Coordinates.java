package it.unimi.alessio.clientsdp.models;

/**
 * Created by alessio on 05/06/17.
 */
public class Coordinates {
    private int x;
    private int y;
    private int gridDimension;
//    public Coordinates(int x, int y) {
//        this.x = x;
//        this.y = y;
//        this.gridDimension = -1;
//    }
    public Coordinates(int x, int y, int gridDimension) {
        this.x = x;
        this.y = y;
        this.gridDimension = gridDimension;
    }
    public Coordinates() {
    }
    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
    //increment x, if it goes over the treshold restart from 0
    public void incrementX(){
        if(gridDimension > 0)
            x = Math.floorMod(++x, gridDimension);
        else
            x++;
    }
    public void decrementX(){
        if(gridDimension > 0)
            x = Math.floorMod(--x, gridDimension);
        else
            x--;
    }
    public void incremetY(){
        if(gridDimension > 0)
            y =  Math.floorMod(++y, gridDimension);
        else
            y++;
    }
    public void decrementY(){
        if(gridDimension > 0)
            y = Math.floorMod(--y, gridDimension);
        else
            y--;
    }
    public Area getArea(){
        if( x < gridDimension/2){
            if(y < gridDimension/2)
                return Area.VERDE;
            else
                return Area.BLUE;
        }
        else{
            if( y < gridDimension/2)
                return Area.ROSSA;
            else
                return Area.GIALLA;
        }
    }
    @Override
    public String toString() {
        return "Coordinates{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Coordinates) {
            int objX =((Coordinates) obj).getX();
            int objY = ((Coordinates) obj).getY();
            return objX == this.x && objY == this.y;
        }
        return false;
    }
}
