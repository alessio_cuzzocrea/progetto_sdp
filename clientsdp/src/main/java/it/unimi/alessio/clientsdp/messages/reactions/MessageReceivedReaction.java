package it.unimi.alessio.clientsdp.messages.reactions;

import com.google.gson.JsonObject;
import it.unimi.alessio.clientsdp.states.MatchContext;

import java.net.Socket;

/**
 * Created by alessio on 20/06/17.
 */
public interface MessageReceivedReaction {
    void executeReaction(MatchContext matchContext, Socket socket, JsonObject jsonMessage);
}
