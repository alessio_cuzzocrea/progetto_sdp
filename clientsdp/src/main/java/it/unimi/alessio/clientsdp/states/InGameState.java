package it.unimi.alessio.clientsdp.states;


import it.unimi.alessio.clientsdp.threads.*;
import it.unimi.alessio.libsdp.simulator.AccelerometerSimulator;

/**
 * Created by alessio on 11/06/17.
 */
public class InGameState implements State {
    @Override
    public void bootstrapState(MatchContext matchContext) {
        new TokenConsumer(matchContext).start();
        new TokenSenderThread(matchContext).start();
        new ServerThread(matchContext).start();
        new UserInputThread(matchContext).start();
        new Thread(new AccelerometerSimulator(matchContext.getMeasurementList())).start();
        new BombGenerator(matchContext.getMeasurementList(), matchContext.getBombList()).start();
    }
}
