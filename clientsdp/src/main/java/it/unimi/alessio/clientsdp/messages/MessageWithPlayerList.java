package it.unimi.alessio.clientsdp.messages;

import it.unimi.alessio.shared.models.Player;

import java.util.List;

/**
 * Created by alessio on 17/06/17.
 */
public class MessageWithPlayerList extends Message {
    private List<Player> playerList;

    public MessageWithPlayerList(MessageType type, Player sender,Player recipient, List<Player> playerList) {
        super(type, sender,recipient );
        this.playerList = playerList;
    }

    public List<Player> getPlayerList() {
        return playerList;
    }

    public void setPlayerList(List<Player> playerList) {
        this.playerList = playerList;
    }

    @Override
    public String toString() {
        return "MessageWithPlayerList{" +
                "type=" + type +
                ", recipient=" + recipient +
                ", playerList=" + playerList +
                '}';
    }
}
