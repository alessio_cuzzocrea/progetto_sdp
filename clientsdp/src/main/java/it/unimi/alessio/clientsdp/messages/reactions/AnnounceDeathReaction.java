package it.unimi.alessio.clientsdp.messages.reactions;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import it.unimi.alessio.clientsdp.messages.Message;
import it.unimi.alessio.clientsdp.messages.MessageType;
import it.unimi.alessio.clientsdp.states.MatchContext;
import it.unimi.alessio.shared.exceptions.GameException;

import java.net.Socket;

public class AnnounceDeathReaction implements MessageReceivedReaction {
    @Override
    public void executeReaction(MatchContext matchContext, Socket socket, JsonObject jsonMessage) {
        Message message = new Gson().fromJson(jsonMessage, Message.class);
        try{
            matchContext.getCurrentGame().removeUser(message.getSender().getUsername());
        }catch (GameException e){}
        Message response = new Message(
                MessageType.DEATH_ACKNOWLEDGE,
                matchContext.getCurrentPlayer(),
                message.getSender()
        );
        matchContext.getMessageStorage().putItem(response);
    }
}