package it.unimi.alessio.clientsdp;


import it.unimi.alessio.clientsdp.models.Coordinates;
import it.unimi.alessio.clientsdp.states.DeathState;
import it.unimi.alessio.clientsdp.states.MatchContext;
import it.unimi.alessio.shared.models.Player;

/**
 * Created by alessio on 02/06/17.
 */
public enum PlayingCommand {
    SUICIDE("suicidio") {
        @Override
        public void execute(MatchContext matchContext) {
            if(!matchContext.setState(new DeathState()))
                System.out.println("sei già morto");
        }
    },
    GO_LEFT("vai a sinistra") {
        @Override
        public void execute(MatchContext matchContext) {
            matchContext.doMove(this);
        }
    },
    GO_RIGHT("vai a destra") {
        @Override
        public void execute(MatchContext matchContext) {
            matchContext.doMove(this);
        }
    },
    GO_UP("vai su") {
        @Override
        public void execute(MatchContext matchContext) {
            matchContext.doMove(this);
        }
    },
    GO_DOWN("vai giù") {
        @Override
        public void execute(MatchContext matchContext) {
            matchContext.doMove(this);
        }
    },
    DROP_BOMB("butta la bomba") {
        @Override
        public void execute(MatchContext matchContext) {
            if (matchContext.getBombList().peekFirst() == null)
                System.out.println("non hai nessuna bomba");
        }
    },
    TELEPORT("teleport"){
        @Override
        public void execute(MatchContext matchContext) {
            Coordinates c = new Coordinates(0,0,matchContext.getCurrentGame().getGridDimension());
            matchContext.setCurrentPosition(c);
        }
    },
    PRINT_PLAYERS("elenco giocatori") {
        @Override
        public void execute(MatchContext matchContext) {
            for (Player p : matchContext.getCurrentGame().getPlayers()) {
                System.out.print(p.getUsername() + " - ");
            }
            System.out.println();
        }
    };

    private String description;

    PlayingCommand(String description) {
        this.description = description;
    }

    public static PlayingCommand valueOf(int ordinal) {
        for (PlayingCommand c : PlayingCommand.values()) {
            if (c.ordinal() == ordinal)
                return c;
        }
        throw new IllegalArgumentException();
    }

    public String getDescription() {
        return description;
    }

    public static void printCommandsMenu() {
        System.out.println("Cosa vuoi fare?");
        for (PlayingCommand command :
                PlayingCommand.values()) {
            System.out.println(command.ordinal() + " - " + command.getDescription());
        }
    }

    public abstract void execute(MatchContext matchContext);
}

