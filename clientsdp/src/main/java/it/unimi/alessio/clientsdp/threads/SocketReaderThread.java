package it.unimi.alessio.clientsdp.threads;

import com.google.gson.*;
import it.unimi.alessio.clientsdp.AppProperties;
import it.unimi.alessio.clientsdp.PropertiesFields;
import it.unimi.alessio.clientsdp.messages.*;
import it.unimi.alessio.clientsdp.messages.reactions.MessageReceivedReaction;
import it.unimi.alessio.clientsdp.states.MatchContext;
import it.unimi.alessio.clientsdp.storages.Storage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 * Created by alessio on 04/06/17.
 */
public class SocketReaderThread extends Thread {
    //expected input from this class:
    //moves
    //tokens
    //peer leaving
    //peer joining
    private Socket socket;
    private BufferedReader inputData;
    private Storage<Message> messageStorage;
    private MatchContext matchContext;

    public SocketReaderThread(Socket socket, MatchContext matchContext) {
        this.socket = socket;
        this.matchContext = matchContext;
        this.messageStorage = matchContext.getMessageStorage();
        try {
            inputData = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.matchContext.getSocketReaderThreadList().add(this);
    }

    @Override
    public void run() {
        boolean exit = false;
        JsonParser jp = new JsonParser();
        try {
            while (!exit) {
                String s = inputData.readLine();
                if (s != null) {
                    JsonObject jsonMessage = (JsonObject) jp.parse(s);
                    String type = jsonMessage.get("type").getAsString();

                    MessageReceivedReaction reaction = matchContext.getReactionTable().get(MessageType.valueOf(type));
                    if(reaction != null)
                        reaction.executeReaction(matchContext, socket,jsonMessage );
                    else
                        System.out.println("reaction not found");

                } else {
                    //connection dropped
                    this.socket.close();
                    boolean debugEnabled = Boolean.parseBoolean(AppProperties.getProperties().getProperty(PropertiesFields.debugMode));
                    if(debugEnabled)
                        System.out.println("socket reader: connection closed");
                    exit = true;
                }
            }
        } catch (IOException e) {
            boolean debugEnabled = Boolean.parseBoolean(AppProperties.getProperties().getProperty(PropertiesFields.debugMode));
            if(debugEnabled)
                System.out.println("socket reader: connection closed");
            this.matchContext.getSocketReaderThreadList().remove(this);
        }
    }

    public void closeSocket() {
        try {
            this.socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
