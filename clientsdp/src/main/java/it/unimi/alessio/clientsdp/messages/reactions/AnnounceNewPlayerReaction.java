package it.unimi.alessio.clientsdp.messages.reactions;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import it.unimi.alessio.clientsdp.messages.MessageType;
import it.unimi.alessio.clientsdp.messages.MessageWithCoordinates;
import it.unimi.alessio.clientsdp.messages.MessageWithPlayer;
import it.unimi.alessio.clientsdp.states.MatchContext;
import it.unimi.alessio.clientsdp.threads.SocketWriterThread;
import it.unimi.alessio.shared.exceptions.GameException;

import java.net.Socket;

/**
 * Created by alessio on 6/20/17.
 */
public class AnnounceNewPlayerReaction implements MessageReceivedReaction {
    @Override
    public void executeReaction(MatchContext matchContext, Socket socket, JsonObject jsonMessage) {
        MessageWithPlayer message = new Gson().fromJson(jsonMessage, MessageWithPlayer.class);
        try {
            matchContext.getCurrentGame().addPlayerAfterAnother(
                    message.getSender(),
                    message.getContainedPlayer()
            );
            new SocketWriterThread(socket, matchContext, message.getSender()).start();
        }catch (GameException e){

        }
        System.out.println(message.getSender().getUsername() + " è entrato in partita");
        MessageWithCoordinates response = new MessageWithCoordinates(
                MessageType.ANNOUNCE_NEW_PLAYER_RESPONSE,
                matchContext.getCurrentPlayer(),
                message.getSender(),
                matchContext.getCurrentPosition()
        );
        matchContext.getMessageStorage().putItem(response);
    }
}
