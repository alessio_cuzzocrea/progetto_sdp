package it.unimi.alessio.clientsdp;

import it.unimi.alessio.clientsdp.states.InitialState;
import it.unimi.alessio.clientsdp.states.MatchContext;
import it.unimi.alessio.clientsdp.states.State;
import java.io.*;
import java.util.Scanner;

/**
 * Created by alessio on 22/05/17.
 */
public class Main {

    public static final String BASE_URI = "http://localhost:8080/gameserver/";

    public static void main(String args[]) {
        AppProperties.getProperties().setProperty(PropertiesFields.debugMode, Boolean.FALSE.toString());
        AppProperties.getProperties().setProperty(PropertiesFields.printConsumingToken, Boolean.FALSE.toString());
        AppProperties.getProperties().setProperty(PropertiesFields.sleepFactor, "0");
        AppProperties.getProperties().setProperty(PropertiesFields.sleepBeforeDie, "0");
        AppProperties.getProperties().setProperty(PropertiesFields.askForGameSpecs, Boolean.FALSE.toString());
        AppProperties.getProperties().setProperty(PropertiesFields.connectToRandomPlayer, Boolean.FALSE.toString());
        AppProperties.getProperties().setProperty(PropertiesFields.addBombs, Boolean.FALSE.toString());
        AppProperties.getProperties().setProperty(PropertiesFields.storageSleep, "0");
        AppProperties.getProperties().setProperty(PropertiesFields.responseStorageSleep, "0");
        MatchContext matchContext = null;
        try {
            matchContext = new MatchContext();
        }catch (IOException e){
            System.out.println("problema nella creazione della server socket");
            System.exit(1);
        }
        State start = new InitialState();
        matchContext.setState(start);
        matchContext.bootstrapState();


        int choice;
        Scanner scan = new Scanner(System.in);
        while (matchContext.getState() instanceof InitialState) {
            MainMenuCommand.printCommandsMenu();
            try {
                choice = Integer.parseInt(scan.nextLine());
                MainMenuCommand.valueOf(choice).execute(scan, matchContext);
            } catch (IllegalArgumentException e) {
                System.out.println("scelta non corrispondente a nessuna opzione");
            }
        }
        matchContext.bootstrapState(); //a questo punto sono in uno stato di join o create

    }
}



