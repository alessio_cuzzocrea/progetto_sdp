package it.unimi.alessio.clientsdp.messages;

import it.unimi.alessio.clientsdp.models.Coordinates;
import it.unimi.alessio.shared.models.Player;

/**
 * Created by alessio on 21/06/17.
 */
public class MessageWithCoordinates extends Message{
    private Coordinates coordinates;

    public MessageWithCoordinates(MessageType type, Player sender, Player recipient, Coordinates coordinates) {
        super(type, sender,recipient);
        this.coordinates = coordinates;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }
}
