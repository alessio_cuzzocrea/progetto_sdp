package it.unimi.alessio.clientsdp;

import it.unimi.alessio.shared.exceptions.GameException;
import it.unimi.alessio.shared.models.Game;
import it.unimi.alessio.shared.models.GameList;
import it.unimi.alessio.shared.models.Player;
import org.eclipse.persistence.jaxb.rs.MOXyJsonProvider;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;

/**
 * Created by alessio on 22/05/17.
 */
public class RestClient {
    private String ipServer, portaServer;
    Client client;
    WebTarget target;

    public RestClient(String ipServer, String portaServer) {
        this.ipServer = ipServer;
        this.portaServer = portaServer;
        client = ClientBuilder.newClient();
        client.register(MOXyJsonProvider.class);
        target = client.target("http://" + ipServer + ":" + portaServer + "/gameserver");
    }

    public void getGames() {
        GameList gl = null;
        try {
            gl = target.path("games").request().get(GameList.class);
        }catch (ProcessingException e){
            System.out.println("server non raggiungibile");
            System.exit(1);
        }
        System.out.println("Lista partite: ");
        for (Game g : gl.gameList.values()) {
            System.out.print(g.getName() + " - ");
        }
        System.out.println();
    }

    public void getGame(String gameName) {
        Response r = null;
        try {
            r = target.path("games").path(gameName).request().get();
        } catch (ProcessingException e) {
            System.out.println("server non raggiungibile");
            System.exit(1);
        }
        if (checkResponse(r)) {
            Game g = r.readEntity(Game.class);
            System.out.println("Dettagli partita:");
            System.out.println("\tnome: " + g.getName());
            System.out.println("\tlato griglia: " + g.getGridDimension());
            System.out.println("\tpunteggio massimo: " + g.getMaxPoints());
            System.out.print("\tLista giocatori: ");
            for (Player p : g.getPlayers()) {
                System.out.print(p.getUsername() + " - ");
            }
            System.out.println();
        }
    }

    public Game createGame(String gameName, int gridSide, int maxPoints, Player player) {
        ArrayList<Player> list = new ArrayList<>();
        list.add(player);
        Game g = new Game(gameName, gridSide, maxPoints, list);
        Response r = null;
        try {
            r = target.path("games").request().post(Entity.entity(g, MediaType.APPLICATION_JSON));
        } catch (ProcessingException e) {
            System.out.println("server non raggiungibile");
            System.exit(1);
        }
        Game returnValue = null;
        if (checkResponse(r))
            returnValue = r.readEntity(Game.class);
        return returnValue;
    }

    public Game putPlayerToGame(String gameName, Player player) {
        Response r = null;
        try {
            r =target.path("games").path(gameName).request().put(Entity.entity(player, MediaType.APPLICATION_JSON));
        }catch (ProcessingException e){
            System.out.println("server non raggiungibile");
            System.exit(1);
        }
        Game returnValue = null;
        if (checkResponse(r))
            returnValue = r.readEntity(Game.class);
        return returnValue;
    }

    public void removePlayerFromGame(String gameName, Player player) {
        Response r = null;
        try{
             r = target.path("games").path(gameName).path(player.getUsername()).request().delete();

        }catch (ProcessingException e){
            System.out.println("server non raggiungibile");
            System.exit(1);
        }
        if (!checkResponse(r)) {
            throw new RuntimeException("giocatore già eliminato");
        }
    }

    public void deleteGame(Game game) {
        Response r = null;
        try {
         r = target.path("games").path(game.getName()).request().delete();
        }catch (ProcessingException e){
            System.out.println("server non raggiungibile");
            System.exit(1);
        }
        if (!checkResponse(r)) {
            throw new RuntimeException("partita già vinta");
        }
    }

    private boolean checkResponse(Response r) {
        if(r == null) {
            System.out.println("nessuna risposta dal server");
            return false;
        }
        if (r.getStatus() == Response.Status.OK.getStatusCode())
            return true;

        if (r.getStatusInfo().getFamily() == Response.Status.Family.CLIENT_ERROR) {
            GameException ge = r.readEntity(GameException.class);
            System.out.println(ge.getErrorDescription());
        } else {
            String error = r.readEntity(String.class);
            System.out.println(error);
        }
        return false;

    }


}
