package it.unimi.alessio.clientsdp.storages;

/**
 * Created by alessio on 14/06/17.
 */
public class TokenStorage {
    private Integer storedToken;
    private final Object storedTokenLock = new Object();
    private boolean tokenReleasable = false;

    public void checkInToken() {
        synchronized (storedTokenLock) {
            while (storedToken == null) {
                try {
                    storedTokenLock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void checkOutToken() {
        synchronized (storedTokenLock) {
            tokenReleasable = true;
            storedToken = null;
            storedTokenLock.notifyAll();
        }
    }

    public void releaseToken() {
        synchronized (storedTokenLock) {
            while (!tokenReleasable) {
                try {
                    storedTokenLock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            tokenReleasable = false;
            storedTokenLock.notifyAll();
        }
    }

    public void putToken() {
        synchronized (storedTokenLock) {
            while (storedToken != null) {
                try {
                    storedTokenLock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            storedToken = 1;
            tokenReleasable = false;
            storedTokenLock.notifyAll();
        }
    }
}

