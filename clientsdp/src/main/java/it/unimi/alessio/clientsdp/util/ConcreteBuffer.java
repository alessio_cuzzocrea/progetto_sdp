package it.unimi.alessio.clientsdp.util;

import it.unimi.alessio.libsdp.simulator.Buffer;
import it.unimi.alessio.libsdp.simulator.Measurement;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alessio on 24/06/17.
 */
public class ConcreteBuffer implements Buffer<Measurement> {
    private ArrayList<Measurement> buffer = new ArrayList<>();
    @Override
    public synchronized void addNewMeasurement(Measurement measurement) {
        this.buffer.add(measurement);
    }

    @Override
    public List readAllAndClean() {
        List tmp = buffer;
        buffer = new ArrayList<Measurement>();
        return tmp;
    }
}
