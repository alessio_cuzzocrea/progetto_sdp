package it.unimi.alessio.clientsdp.messages;

import it.unimi.alessio.shared.models.Player;

/**
 * Created by alessio on 16/06/17.
 */
public class MessageWithPlayer extends Message {
    private Player containedPlayer;
    public MessageWithPlayer(MessageType type, Player sender, Player recipient, Player containedPlayer) {
        super(type, sender, recipient);
        this.containedPlayer = containedPlayer;
    }

    public Player getContainedPlayer() {
        return containedPlayer;
    }

    public void setContainedPlayer(Player containedPlayer) {
        this.containedPlayer = containedPlayer;
    }
}
