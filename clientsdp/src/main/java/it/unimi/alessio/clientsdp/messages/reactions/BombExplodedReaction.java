package it.unimi.alessio.clientsdp.messages.reactions;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import it.unimi.alessio.clientsdp.messages.MessageType;
import it.unimi.alessio.clientsdp.messages.MessageWithBombExploded;
import it.unimi.alessio.clientsdp.states.DeathState;
import it.unimi.alessio.clientsdp.states.MatchContext;

import java.net.Socket;

/**
 * Created by alessio on 26/06/17.
 */
public class BombExplodedReaction implements MessageReceivedReaction {
    @Override
    public void executeReaction(MatchContext matchContext, Socket socket, JsonObject jsonMessage) {
        MessageWithBombExploded message = new Gson().fromJson(jsonMessage, MessageWithBombExploded.class);
        if(matchContext.getCurrentArea().equals(message.getBombExploded().getBomb().getArea()) &&
                matchContext.setState(new DeathState()))
        {
            System.out.println("sei morto per la bomba di: " + message.getSender().getUsername());
            MessageWithBombExploded response = new MessageWithBombExploded(
                    MessageType.BOMB_DEATH_OCCURRED,
                    matchContext.getCurrentPlayer(),
                    message.getSender(),
                    message.getBombExploded()

            );
            matchContext.getMessageStorage().putItem(response);
        }
    }
}
