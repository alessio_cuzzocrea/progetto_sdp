package it.unimi.alessio.shared.exceptions;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by alessio on 01/06/17.
 */
@XmlRootElement
public class GameException extends RuntimeException {
    @XmlElement
    private int errorCode;
    @XmlElement
    private String errorDescription;

    public GameException(ErrorCodes.GameService errorCode){
        super(Integer.toString(errorCode.ordinal()));
        this.errorCode = errorCode.ordinal();
        this.errorDescription = errorCode.getDescription();
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public GameException(){}
}
