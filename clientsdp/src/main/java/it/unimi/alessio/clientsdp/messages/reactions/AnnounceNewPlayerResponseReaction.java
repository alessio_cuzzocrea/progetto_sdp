package it.unimi.alessio.clientsdp.messages.reactions;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import it.unimi.alessio.clientsdp.messages.Message;
import it.unimi.alessio.clientsdp.messages.MessageWithCoordinates;
import it.unimi.alessio.clientsdp.states.MatchContext;

import java.net.Socket;

/**
 * Created by alessio on 21/06/17.
 */
public class AnnounceNewPlayerResponseReaction implements MessageReceivedReaction {
    @Override
    public void executeReaction(MatchContext matchContext, Socket socket, JsonObject jsonMessage) {
        MessageWithCoordinates m  = new Gson().fromJson(jsonMessage, MessageWithCoordinates.class);
            matchContext.getResponseMessageStorage().putResponse(m);
    }
}
