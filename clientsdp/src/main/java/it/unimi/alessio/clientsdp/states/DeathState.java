package it.unimi.alessio.clientsdp.states;

import java.io.IOException;

/**
 * Created by alessio on 23/06/17.
 */
public class DeathState implements State {
    @Override
    public void bootstrapState(MatchContext matchContext) {
        try {
            matchContext.getServerSocket().close();
            matchContext.getTmpNextPlayerQueue().clear();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
